>-- Expression like this 5+2+5+6+7*8 etc simple

>evaluate (e:str) = show (eval e)

>eval (Anum n) = n
>eval (Aplus e1 e2) = (eval e1) + (eval e2)
>eval (Aminus e1 e2)= (eval e1) - (eval e2)
>eval (Amult e1 e2) = (eval e1) * (eval e2)
>eval (Apow e1 e2)  = (eval e1) ^ (eval e2)
>eval (Adiv e1 e2)  = div (eval e1) (eval e2)

>show' (e:str) = show e

>expr str = display (expression str)
>display ((Aerror str):ls) = putStrLn ("\n"++str)
>display str = putStrLn $ "\n"++(show' str)++"\n\nAns-["++(evaluate str)++"]\n"
>expression str =nextpro  (concat $ exp' (remove' str ' ')) str
>nextpro str ex | str=="" = (maketree (remove' ex ' ')) | otherwise =[Aerror str]
>maketree str =maketree' $ tokenize $ removebl $ toknize' str
>maketree' str = maketree'' (checkcons str) str
>maketree'' val str | val=="" = total str | otherwise = [Aerror val]
>total str =  both (tmult (tdiv (tpow (letsget str []) []) []) []) []

>--Checking For consecutive number present or not
>checkcons [e] = ""
>checkcons (Tnum e:Tnum e':ls) = "Error:Operator not present in between numbers.["++(show e)++"\t"++(show e')++"].\n"
>checkcons (e:l:ls) = checkcons (l:ls)

>--multiplepar using stack
>letsget [] temp = temp
>letsget (e:ls) temp|e==Tob =  temp++(letsget' ls (pushstack [] e))
>                   |otherwise = letsget ls (temp++[e])
>letsget' [] stack = stack
>letsget' (e:ls) stack | e==Tcb = letsget' ls (popstack stack)
>                      | e==Tob = letsget' ls (pushstack stack e)
>                      |otherwise = letsget' ls (pushstack stack e)
>--Push in the stack
>pushstack stack e = stack++[e]
>popstack stack = popstack' (reverse stack) []
>popstack' (e:stack) temp | e==Tob =((reverse stack)++(total (reverse temp)))
>                          |otherwise = popstack' stack (temp++[e])
>both [] temp = temp
>both (e:ls) temp | (e==Tplus)||(e==Tminus) = both (rmfirst ls) ((rmlast temp)++(construct e (getlast'' temp) (head ls)))
>                 | otherwise = both ls (temp++[e])
>tmult [] temp = temp
>tmult (e:ls) temp | e==Tmult = tmult (rmfirst ls) ((rmlast temp)++(construct Tmult (getlast'' temp) (head ls)))
>                  | otherwise = tmult ls (temp++[e])
>tdiv [] temp = temp
>tdiv (e:ls) temp | e==Tdiv = tdiv (rmfirst ls) ((rmlast temp)++(construct Tdiv (getlast'' temp) (head ls)))
>                 |otherwise = tdiv ls (temp++[e])
>tpow [] temp = temp
>tpow (e:ls) temp | e==Tpow = tpow (rmfirst ls) ((rmlast temp)++(construct Tpow (getlast'' temp) (head ls)))
>                 | otherwise = tpow ls (temp++[e])
>construct e (Tnum e') (Tnum e'') = construct' e (Anum e') (Anum e'')
>construct e (Tnum e') t          = construct' e (Anum e') t
>construct e l (Tnum e')          = construct' e l (Anum e')
>construct e l t                  = construct' e l t
>construct' e l t| e==Tpow = [Apow l t]
>                | e==Tdiv = [Adiv l t]
>                | e==Tmult = [Amult l t]
>                | e==Tplus = [Aplus l t]
>                | e==Tminus = [Aminus l t]
>                | otherwise = [Aerror "Error: Does Not match."]
>--gettcb [] temp= []
>makestr str = map f str
>f e = show e
>str2i str = read str::Integer
>getfirst' ts = head ts
>getlast'' ts = last ts
>addLast' [] e = [e]
>addLast' (e':ls) e =e':addLast' ls e
>rmfirst (e:ls) = ls
>rmlast [] = []
>rmlast (e:ls) | ls==[] = []|otherwise = e:rmlast ls
>str2Num  str =read str::Integer
>--these functions return the getfirst return firstvalue of list and getlast return last value of list
>getfirst [] = " "
>getfirst (e:ls) = e
>getlast [] tem = tem
>getlast (ls:xs) tem= getlast xs ls

>data Aexp = Aplus Aexp Aexp |
>            Amult Aexp Aexp |
>            Adiv Aexp Aexp |
>            Aminus Aexp Aexp |
>            Apow Aexp Aexp |
>            Anum Integer |
>            Aerror [Char]|
>            Apar Aexp |
>            Tnum Integer |
>            Tpow |
>            Tplus |
>            Tminus |
>            Tmult |
>            Tdiv |
>            Tspace | 
>            Terror [Char]|
>            Tob |
>            Tcb deriving (Eq, Show)


>--data Token = Tnum Integer| Tpow | Tplus | Tminus | Tmult | Tdiv | Tspace | Terror [Char] | Tob | Tcb deriving (Eq, Show)
>tokenize :: [[Char]] -> [Aexp]
>tokenize [] = []
>tokenize ("^":ts) = Tpow : tokenize ts
>tokenize ("+":ts) = Tplus : tokenize ts
>tokenize ("-":ts) = Tminus : tokenize ts
>tokenize ("*":ts) = Tmult : tokenize ts
>tokenize ("/":ts) = Tdiv : tokenize ts
>tokenize ("(":ts) = Tob : tokenize ts
>tokenize (")":ts) = Tcb : tokenize ts
>tokenize (t:ts)   | elem (head t) "0123456789" =(Tnum (str2i t)):tokenize ts
>                  | otherwise = (Terror t):(tokenize ts)

>--remove blank function To remove blank string in list of string
>removebl [] = []
>removebl (ls:str) | ls=="" = removebl str |ls==" "=removebl str | otherwise = ls:removebl str 
>--This function Tokenize expression to each block of string
>toknize'::[Char]->[[Char]]
>toknize' str =tok str [] []
>tok [] xs pq= [xs]++[pq]
>tok (e:ls) xs pq| (elem e ['0'..'9']) =pq:tok ls (xs++[e]) []
>                | e=='+' = xs:[e]:tok ls [] pq
>                | e=='-' = xs:[e]:tok ls [] pq
>                | e=='*' = xs:[e]:tok ls [] pq
>                | e=='/' = xs:[e]:tok ls [] pq
>                | e=='^' = xs:[e]:tok ls [] pq
>                | e=='(' = xs:[e]:tok ls [] pq
>                | e==')' = xs:[e]:tok ls [] pq
>                | e==' ' = xs:[e]:tok ls [] pq
>                | e=='=' = xs:[e]:tok ls [] pq
>                | (elem e ['a'..'z']) = xs:[e]:tok ls [] pq
>                | otherwise =tok ls (xs++[e]) (pq)



>--Function Work To validate Expression
>exp' str =validate str (checkstr str)
>validate str xs | xs=="True"= validtk (removebl (toknize' str))
>                | otherwise = ["Error: Expression Cannot validate.Invalid string present-("++xs++")\n"]




>validtk str = validateoprand (opernd str str [] 1) (checkparan str)
>validateoprand str par | ((concat str)=="")&&((concat par)=="") =   [""]  | otherwise = str++par

>opernd [] str xs c= []
>opernd (e:ls) str xs c| e=="+" = (validplus (getlast xs " ") ls c):opernd ls str [] (c+1)
>                      | e=="-" = (validplus (getlast xs " ") ls c):opernd ls str [] (c+1)
>                      | e=="/" = (validplus (getlast xs " ") ls c):opernd ls str [] (c+1)
>                      | e=="*" = (validplus (getlast xs " ") ls c):opernd ls str [] (c+1)
>                      | e=="^" = (validplus (getlast xs " ") ls c):opernd ls str [] (c+1)
>                      | otherwise = opernd ls str (xs++[e]) (c+1)

>validplus str ls c| str == " " = "Error: argument not passed to operator.\n"
>                  | otherwise = validplus' str (getfirst ls) c
>
>validplus' str ls c| (ls==" ")||(ls==")")||(str=="(") = "Error: argument not passed to operator last.\n"
>                   | (str==")")&&(ls=="(") = validplus'' "" ""
>                   | (str==")") = validplus'' "" (checkint ls c)
>                   | (ls=="(")||(ls==")") = validplus'' (checkint str c) ""
>                   | otherwise = validplus'' "" ""
>validplus'' str ls = str++ls
>--Checking Integer or not
>checkint [] c= ""
>checkint (e:ls) c| ismember e ['0'..'9'] = checkint ls c
>                 | otherwise = "ssdiError: missplace operator (+)and ("++[e]++") pos-{"++show c++"}\n"
>

>--Checkpar function input should be (exp) validate output
>checkparan str = checkparan' ((length (filter (=="(") str))+(length (filter (==")") str)))
>checkparan' len | (rem len 2)==0 = [""] 
>              | otherwise = ["Error: paranthesis are not match\n"]

>appn' str = map (f) str
>  where 
>  f a = a++"\n"

>--check string function to check the string present or not in the expression
>checkstr str =valid' str ['a'..'z'] []
>valid' [] xs [] = "True"
>valid' [] xs pq = pq
>valid' (l:ls) xs pq| (ismember l xs) = valid' ls xs (pq++[l])
>                   | otherwise = valid' ls xs pq
>ismember l [] = False
>ismember l (e:ls) | l==e = True | otherwise = ismember l ls







>--Removing multiple '\n' or \t int the input File --It removes Extra spaces \n\n etc in the file remove.
>remove str = remove' str '\n'
>remove' str e = remove'' str e []
>remove'' [] e xs = xs
>remove'' (x:ls) e xs | (x==e) =(nextIteration ls e (xs))
>                     | otherwise = remove'' ls e (xs++[x])

>nextIteration [] e xs = remove'' [] e xs
>nextIteration (e1:ls) e xs |(check e1 e) = checkxs (getlast' xs) e ls xs
>                          |otherwise = checkxs' (getlast' xs) e1 e ls xs

>checkxs' n e1 e ls xs | n==e = remove'' ls e (xs++[e1]) 
>                      | (n=='\n') && (e==' ')= remove'' ls e (xs++[e1])
>                      |  otherwise = remove'' ls e (xs++[e]++[e1])
>--Its Take a Last Element of List
>getlast' [] = ' '
>getlast' xs =(last xs)
>--Check Already escapr sequence present or not in the list
>checkxs e1 e ls xs| e1==e = remove'' ls e xs
>                  |(e1=='\n') && (e==' ') = remove'' ls e xs
>                  | otherwise = remove'' ls e (xs++[e])
>check e1 e | e1==e = True | otherwise = False

>--These functions convert list of string to number
>digit str =  digit' (concat' [(cc str)])
>digit' (e:ls) = e
>concat' [] = []
>concat' (e:ls) =(concat'' e 1 0):concat' ls
>concat'' [e] c ans|e==0 = ((rev' ans 0)*10) | otherwise = (rev' (ans+(e*(mul' c))) 0)
>concat'' (e:ls) c ans=concat'' ls (c+1) (ans+(e*(mul' c)))
>cc [] = []
>cc (e:ls) =(cc' e): cc ls
>cc' e | '1'==e =1| '9'==e =9| '8'==e =8| '7'==e =7| '6'==e =6| '5'==e =5| '4'==e =4| '3'==e =3| '2'==e =2| otherwise=0
>rev' 0 c = c
>rev' n c =rev' (div n 10) ((c*10)+(rem n 10))
>rev'' (e:[])=rev' e 0
>mul' c |c==1 = c | c==2 = 10 | c==3 = 100 | c==4 = 1000|c==5 = 10000|c==6=100000|c==7=1000000|otherwise = undefined

closetcb [] temp = temp
closetcb (e:ls) temp| e==Tcb   = closetcb ls []
                    |otherwise = closetcb ls (temp++[e])


gettcb (e:ls) temp| e==Tob = temp++(constructapr' (gettcb' ls []))
                  | otherwise = gettcb ls (temp++[e])
gettcb' (e:ls) temp  | e==Tcb = gettcb' ls (constructapr' temp)
                     | e==Tob = (constructapr' temp++(gettcb'' ls []))
                     |otherwise = gettcb' ls (temp++[e])
gettcb'' (e:ls) temp | e==Tob = gettcb' (e:ls) []
                     | e==Tcb = temp
                     |otherwise = gettcb'' ls (temp++[e])

--constructapr [] = []
constructapr' (str) = [Apar str]
fun [] temp = temp
fun (e:st) temp | e==Tob = temp++(constructapr' $ getclose st [])
                | otherwise = fun st (temp++[e])
getclose (e:ls) temp | e==Tob = temp++(fun (e:ls) [])
                     | e==Tcb = temp++[e]++(fun ls [])
                     |otherwise = getclose ls (temp++[e])

