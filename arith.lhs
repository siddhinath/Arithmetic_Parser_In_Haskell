>expression str = validate $ tokenize $ removebl $ toknize' str


>rmlast (e:ls) | ls==[] = []|otherwise = e:rmlast ls

>tryPlus (Tplus:ts) xs pq=addLast pq (tryPlus' (getlast' xs) (getfirst ts))  --((Aplus (Anum 5) (Anum 5)):pq)
>tryPlus (e:ts) xs pq= tryPlus ts (addLast xs e) pq
>tryPlus' (Tnum e) (Tnum e') = Aplus (Anum e) (Anum e')
>tryPlus' (Tnum e) r = Aerror "Error: Argument error\n"
>tryPlus' r (Tnum e) = Aerror "Error: Argument error\n"
>addLast [] e = [e]
>addLast (e':ls) e =e':addLast ls e
>getNum (Tnum e) = e

>getfirst [] = Terror "Error: Argument not pass.\n"
>getfirst ts = head ts
>getlast' [] = Terror "Error: Argument not pass.\n"
>getlast' ts = last ts


>data Aexp = Aplus Aexp Aexp |
>            Amult Aexp Aexp |
>            Adiv Aexp Aexp |
>            Aminus Aexp Aexp |
>            Apow Aexp Aexp |
>            Anum Integer |
>            Aerror [Char]|
>            Apar Aexp deriving (Eq, Show)

>parse ts = parse' ts []
>parse' [] xs = Aerror ""
>parse' (Tnum n:ts) xs = parse' ts ((Tnum n):xs)




>data Token = Tnum Integer| Tpow | Tplus | Tminus | Tmult | Tdiv | Tspace | Terror [Char] | Tob | Tcb deriving (Eq, Show)
>tokenize :: [[Char]] -> [Token]
>tokenize [] = []
>tokenize ("^":ts) = Tpow : tokenize ts
>tokenize ("+":ts) = Tplus : tokenize ts
>tokenize ("-":ts) = Tminus : tokenize ts
>tokenize ("*":ts) = Tmult : tokenize ts
>tokenize ("/":ts) = Tdiv : tokenize ts
>tokenize ("(":ts) = Tob : tokenize ts
>tokenize (")":ts) = Tcb : tokenize ts
>tokenize (t:ts)   | elem (head t) "0123456789" =(Tnum (str2i t)):tokenize ts
>                  | otherwise = (Terror t):(tokenize ts)

>str2i str = read str::Integer

>--This function Tokenize expression to each block of string
>toknize'::[Char]->[[Char]]
>toknize' str =tok str [] []
>tok [] xs pq= [xs]++[pq]
>tok (e:ls) xs pq| (elem e ['0'..'9']) =pq:tok ls (xs++[e]) []
>                | e=='+' = xs:[e]:tok ls [] pq
>                | e=='-' = xs:[e]:tok ls [] pq
>                | e=='*' = xs:[e]:tok ls [] pq
>                | e=='/' = xs:[e]:tok ls [] pq
>                | e=='^' = xs:[e]:tok ls [] pq
>                | e=='(' = xs:[e]:tok ls [] pq
>                | e==')' = xs:[e]:tok ls [] pq
>                | e==' ' = xs:[e]:tok ls [] pq
>                | (elem e ['a'..'z']) = xs:[e]:tok ls [] pq
>                | otherwise =tok ls (xs++[e]) (pq)



>--remove blank function To remove blank string in list of string
>removebl [] = []
>removebl (ls:str) | ls=="" = removebl str |ls==" "=removebl str | otherwise = ls:removebl str 





